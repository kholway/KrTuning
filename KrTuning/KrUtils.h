#ifndef KrTuning_KrUtils_H
#define KrTuning_KrUtils_H

namespace Kr {

    int getAbsoluteStrawLayerNumberBarrel(int sl, int layer);  //!
    int getAbsoluteStrawLayerNumberEndcap(int sl, int wheel);  //!
    bool isBarrelShortStraw(int lay, int SL); //!
    bool isArRegion(int bec, int lay, int phimod); //!
    bool isKrRegion(int bec, int lay, int phimod); //!

}

#endif
