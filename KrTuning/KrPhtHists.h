#ifndef KrTuning_KrPhtHists_H
#define KrTuning_KrPhtHists_H

#include "TRTFramework/TRTAnalysis.h"

class KrPhtHists : public TRTAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!



public:
  // this is a standard constructor
  KrPhtHists();
  virtual ~KrPhtHists();

  // these are the functions inherited from TRTAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();

  void loopOverTRTHits( const xAOD::TrackParticle *track );


  // this is needed to distribute the algorithm to the workers
  ClassDef(KrPhtHists, 1);
};

#endif // KrTuning_KrPhtHists_H
