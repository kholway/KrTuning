#include "KrTuning/KrUtils.h"

#include <TSystem.h>
#include <TTreeFormula.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TMath.h>
#include <TH1.h>
#include <TH1F.h>
#include <TString.h>


namespace Kr {

    int getAbsoluteStrawLayerNumberBarrel(int sl, int layer)
    {
        int absolute_straw_layer = sl;
        if (layer > 0)
            absolute_straw_layer += 19;
        if (layer > 1)
            absolute_straw_layer += 24;
        if (layer > 2 || absolute_straw_layer > 72)
            Info("getAbsoluteStrawLayerNumberBarrel()",  "unexpected absolute_straw_layer %i for barrel",absolute_straw_layer);
        return absolute_straw_layer;
    }

    int getAbsoluteStrawLayerNumberEndcap(int sl, int wheel)
    {
        int absolute_straw_layer = sl;
        if (wheel < 6)
            absolute_straw_layer += 16 * wheel;
        else
            absolute_straw_layer += 96 + (wheel - 6) * 8;
        if (absolute_straw_layer > 159)
            Info("getAbsoluteStrawLayerNumberEndcap()",  "unexpected absolute straw layer %i for endcap",absolute_straw_layer);
        return absolute_straw_layer;
    }

    // In barrel layer 0, straws 0-8 are short and 9-18 are long
    bool isBarrelShortStraw(int lay, int SL)
    {
        // assume bec is +1 or -1, a barrel straw
        return lay==0 && SL<=8;
    }

    // Hard-coded for 2015 Kr data geometry
    bool isArRegion(int bec, int lay, int phimod)
    {
        bool isit = false;

        switch (bec) {
        case -2: // ECC
            break;
        case -1: // BA
        case 1:  // BA
            isit = lay==0 && phimod<=27;
            break;
        case 2:  // ECA
            isit = lay==5;
            break;
        }

        return isit;
    }

    // Hard-coded for 2015 Kr data geometry
    bool isKrRegion(int bec, int lay, int phimod)
    {
        bool isit = false;

        switch (bec) {
        case -2: // ECC
            isit = lay==3;
            break;
        case -1: // BA
        case 1:  // BA
            isit = lay==0 && phimod>27;
            break;
        case 2:  // ECA
            break;
        }

        return isit;
    }

}
