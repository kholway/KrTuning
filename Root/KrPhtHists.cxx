#include "KrTuning/KrPhtHists.h"
#include "KrTuning/KrUtils.h"
#include "TRTFramework/TRTIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(KrPhtHists)

KrPhtHists::KrPhtHists()
: TRTAnalysis()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



KrPhtHists::~KrPhtHists()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode KrPhtHists::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

    // Event info

    histoStore()->createTH1F("h_NPV", 30, 0, 30, "; N_{PV}; Events");
    histoStore()->createTH1F("h_averageMu", 30, 0, 30, "; #LT#mu#GT; Events");

    // HT prob for entire detector

    histoStore()->createTProfile("h_pHT_eta", 11, 0, 2.2, "; |#eta|; HT Probability");
    histoStore()->createTProfile("h_pHTMB_eta", 11, 0, 2.2, "; |#eta|; HT_{MB} Probability");

    histoStore()->createTProfile("h_pHT_gas", 3,0,3, "; Gas Type; HT Probability");
    histoStore()->createTProfile("h_pHTMB_gas", 3,0,3, "; Gas Type; HT_{MB} Probability");

    // End Caps

    histoStore()->createTProfile("h_gastype_SL_ECC", 159,0,159, "; Straw Layer; Gas Type");

    histoStore()->createTProfile("h_pHT_SL_ECC", 159,0,159, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_ECC", 159,0,159,"; Straw Layer; HT_{MB} Probability");

    histoStore()->createTProfile("h_gastype_SL_ECA", 159,0,159, "; Straw Layer; Gas Type");
    histoStore()->createTProfile("h_pHT_SL_ECA", 159,0,159, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_ECA", 159,0,159,"; Straw Layer; HT_{MB} Probability");

    histoStore()->createTProfile("h_pHT_SL_ECAWheelsKr", 159,0,159, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_ECAWheelsKr", 159,0,159,"; Straw Layer; HT_{MB} Probability");

    // Barrel

    histoStore()->createTProfile("h_gastype_SL_BA", 72,0,72, "; Straw Layer; Gas Type");

    histoStore()->createTProfile("h_pHT_SL_BA", 72,0,72, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_BA", 72,0,72,"; Straw Layer; HT_{MB} Probability");

    histoStore()->createTProfile("h_pHT_SL_BAKrSec", 72,0,72, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_BAKrSec", 72,0,72,"; Straw Layer; HT_{MB} Probability");

    histoStore()->createTProfile("h_pHT_SL_BarShortKr", 72,0,72, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_BarShortKr", 72,0,72,"; Straw Layer; HT_{MB} Probability");

    histoStore()->createTProfile("h_pHT_SL_BarLongKr", 72,0,72, "; Straw Layer; HT Probability");
    histoStore()->createTProfile("h_pHTMB_SL_BarLongKr", 72,0,72,"; Straw Layer; HT_{MB} Probability");



    

    
    return EL::StatusCode::SUCCESS;
}


EL::StatusCode KrPhtHists::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  TRTAnalysis::execute();  // Must Keep This Line

  // Remove Duplicate Events
  if ( isDuplicate() ) return EL::StatusCode::SUCCESS;

  // GRL is irrelevant for the tuning Particle Gun samples
  // if ( not passGRL() ) return EL::StatusCode::SUCCESS;

  // Remove Events with Zero Weight
  if ( hasZeroWeight() ) return EL::StatusCode::SUCCESS;

  // Your Code Goes Here
  //-------------------------------------------------

  // Check config setup
  TString sampleName    = config()->getStr("SampleName");
  // TString inputFile     = config()->getStr("InputFile");
  // TString inputFileList = config()->getStr("InputFileList");
  // TString outputDir     = config()->getStr("OutputDir");
  // TString gridDS        = config()->getStr("GridDS");
  // TString outputDS      = config()->getStr("OutputDS");

  // Chooseif electron or muon selection.... (t/f)
  // NOTE: should I move this to a setup part of the script so it runs only once?
  bool useElectrons(false), useMuons(false);

  if ( sampleName.Contains("el") ) {
      useElectrons = true;
      
      // Sanity check
      // if ( sampleName.Contains("mu")
      //      || inputFile.Contains("mu")
      //      || inputFileList.Contains("mu")
      //      || outputDir.Contains("mu")
      //      || gridDS.Contains("mu")
      //      || outputDS.Contains("mu") )
      //     TRT::fatal( "Naming mismatch (should be either el or mu)");
  }
  if ( sampleName.Contains("mu") ) {
      useMuons = true;
      // Sanity check
      // if ( sampleName.Contains("el")
      //      || inputFile.Contains("el")
      //      || inputFileList.Contains("el")
      //      || outputDir.Contains("el")
      //      || gridDS.Contains("el")
      //      || outputDS.Contains("el") )
      //     TRT::fatal( "Naming mismatch (should be either el or mu)");
  }

  // Get Lepton Containers
  xAOD::ElectronContainer electrons = lepHandler()->getSelectedElectrons();
  xAOD::MuonContainer         muons = lepHandler()->getSelectedMuons();
  if ( electrons.size() == 0 && muons.size() == 0 ) return EL::StatusCode::SUCCESS;

  // Some Histograms
  histoStore()->fillTH1F( "h_NPV", NPV(), weight() );
  histoStore()->fillTH1F( "h_averageMu", averageMu(), weight() );

  // Loops over particles and hits below copied directly from
  if ( useElectrons ) {
      // Loop Over Electrons
      for( auto electron: electrons ) {
          const xAOD::TrackParticle* track = lepHandler()->getTrack( electron );
          if ( not lepHandler()->passSelection( track ) ) continue;
          loopOverTRTHits(track);
      }
  }

  if ( useMuons ) {
      // Loop Over Muons
      for( auto muon: muons ) {
          const xAOD::TrackParticle* track = lepHandler()->getTrack( muon );
          if ( not lepHandler()->passSelection( track ) ) continue;
          loopOverTRTHits(track);
      }
  }

  return EL::StatusCode::SUCCESS;
}



void KrPhtHists::loopOverTRTHits( const xAOD::TrackParticle *track )
{
  // Use this function to loop over TRT hits
  // Remove this entire function if not needed.

  // Get measurements on track
  typedef std::vector<ElementLink<xAOD::TrackStateValidationContainer>> MeasurementsOnTrack;
  if (not track->isAvailable<MeasurementsOnTrack>("msosLink"))
    TRT::fatal("No MSOS called msosLink is available on track");
  const MeasurementsOnTrack& measurementsOnTrack = track->auxdata< MeasurementsOnTrack >("msosLink");

  // Loop over track hits
  for ( auto msos_itr: measurementsOnTrack ) {

      if (not msos_itr.isValid()) continue;

      // skip non-TRT hits
      const xAOD::TrackStateValidation *msos = *msos_itr;

      if( msos->detType() != 3 || msos->type() != 0 ) continue;

      // get drift circle
      if (not msos->trackMeasurementValidationLink().isValid()) continue;
      const xAOD::TrackMeasurementValidation *TRTDriftCircle =  *(msos->trackMeasurementValidationLink());

      // check if HT is passed
      bool isHT    = TRTDriftCircle->auxdata<char>("highThreshold");
      bool isHTMB  = (TRTDriftCircle->auxdata<unsigned int>("bitPattern") & 0x20000);

      // Get other variables
      // float HT = TRTDriftCircle->isAvailable< char >("highThreshold") ? 
      //     TRTDriftCircle->auxdata< char >("highThreshold"): -1;
      int bec = TRTDriftCircle->isAvailable< int >("bec") ?
          TRTDriftCircle->auxdata< int >("bec") : -999.;
      int gastype = TRTDriftCircle->isAvailable< char >("gasType") ?
          TRTDriftCircle->auxdata< char >("gasType") : -1;
      int lay = TRTDriftCircle->isAvailable< int >("layer") ?
          TRTDriftCircle->auxdata< int >("layer") : -999;
      int SL  = TRTDriftCircle->isAvailable< int >("strawlayer") ?
          TRTDriftCircle->auxdata< int >("strawlayer") : -999;
      int phi_module  = TRTDriftCircle->isAvailable< int >("phi_module") ?
          TRTDriftCircle->auxdata< int >("phi_module") : -999;

      // Sanity check HT settings
      if(gastype==2) {
          //Info("loopOverTrtHits()",  "Kr straw  bec: %i, layer: %i, HT: %f", bec,lay,HT);
      }

      // General pHT histograms
      histoStore()->fillTProfile("h_pHT_eta", fabs(track->eta()), isHT, weight());
      histoStore()->fillTProfile("h_pHTMB_eta", fabs(track->eta()), isHTMB, weight());
      histoStore()->fillTProfile("h_pHT_gas", gastype, isHT, weight());
      histoStore()->fillTProfile("h_pHTMB_gas", gastype, isHTMB, weight());

      // pHT (and other) histograms by region 
      int abslayer;
      if (abs(bec)==2) { // Endcaps
          abslayer = Kr::getAbsoluteStrawLayerNumberEndcap(SL,lay);

          if (bec < 0) { // Side C
              histoStore()->fillTProfile("h_gastype_SL_ECC",
                                         abslayer,gastype,weight());
              histoStore()->fillTProfile("h_pHT_SL_ECC",
                                         abslayer,isHT,weight());
              histoStore()->fillTProfile("h_pHTMB_SL_ECC",
                                         abslayer,isHTMB,weight());

          } else { // Side A 

              histoStore()->fillTProfile("h_gastype_SL_ECA",
                                         abslayer,gastype,weight());
              histoStore()->fillTProfile("h_pHT_SL_ECA",
                                         abslayer,isHT,weight());
              histoStore()->fillTProfile("h_pHTMB_SL_ECA",
                                         abslayer,isHTMB,weight());
          }
          
          if(gastype==2) { // Filled with Kr (either side)

              if (lay<=5) { // Wheel Type A (either side)
                  histoStore()->fillTProfile("h_pHT_SL_ECAWheelsKr",
                                             abslayer,isHT,weight());
                  histoStore()->fillTProfile("h_pHTMB_SL_ECAWheelsKr",
                                             abslayer,isHTMB,weight());
                  
              } else { // Wheel Type B (either side)
                  // Nothing for now
              }
          }
      } else if (abs(bec)==1) { // Barrel

          abslayer = Kr::getAbsoluteStrawLayerNumberBarrel(SL,lay);

          histoStore()->fillTProfile("h_gastype_SL_BA",
                                     abslayer,gastype,weight());
          histoStore()->fillTProfile("h_pHT_SL_BA",
                                     abslayer,isHT,weight());
          histoStore()->fillTProfile("h_pHTMB_SL_BA",
                                     abslayer,isHTMB,weight());

          // Kr angular sector of barrel
          // (phi modules 28,29,30,31)
          if (Kr::isKrRegion(1, 0, phi_module)) {
              histoStore()->fillTProfile("h_pHT_SL_BAKrSec",
                                         abslayer,isHT,weight());
              histoStore()->fillTProfile("h_pHTMB_SL_BAKrSec",
                                         abslayer,isHTMB,weight());
          }

          if(gastype==2) { // Filled with Kr
              if ( Kr::isBarrelShortStraw(lay, SL) ) { 
                  // Short barrel straws
                  histoStore()->fillTProfile("h_pHT_SL_BarShortKr",
                                             abslayer,isHT,weight());
                  histoStore()->fillTProfile("h_pHTMB_SL_BarShortKr",
                                             abslayer,isHTMB,weight());
              } else {
                  // Long barrel straws
                  histoStore()->fillTProfile("h_pHT_SL_BarLongKr",
                                             abslayer,isHT,weight());
                  histoStore()->fillTProfile("h_pHTMB_SL_BarLongKr",
                                             abslayer,isHTMB,weight());
              }
          }
      } else {
          Info("execute()",  "Unexpectedval of bec: %i ", bec);
          break;
      } // bec

  }
}

