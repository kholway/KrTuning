#include "KrTuning/Kinematics.h"
#include "TRTFramework/TRTIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(Kinematics)



Kinematics::Kinematics()
: TRTAnalysis()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



Kinematics::~Kinematics()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode Kinematics::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

    histoStore()->createTH1F("h_NPV", 30, 0, 30, "; N_{PV}; Events");
    histoStore()->createTH1F("h_averageMu", 30, 0, 30, "; #LT#mu#GT; Events");

    histoStore()->createTH1F("h_trkPt", 50, 0, 10, "; Track p_{T} [GeV]; Events");

    histoStore()->createTH2F("h_pt_eta_el", 120, -3, 3, 100, 0, 25, "; #eta; Track p_{T} [GeV]");
    histoStore()->createTH2F("h_pt_eta_mu", 120, -3, 3, 100, 0, 25, "; #eta; Track p_{T} [GeV]");

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode Kinematics::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  TRTAnalysis::execute();  // Must Keep This Line

  // Remove Duplicate Events
  if ( isDuplicate() ) return EL::StatusCode::SUCCESS;

  // GRL is irrelevant for the tuning Particle Gun samples
  // if ( not passGRL() ) return EL::StatusCode::SUCCESS;

  // Remove Events with Zero Weight
  if ( hasZeroWeight() ) return EL::StatusCode::SUCCESS;

  // Your Code Goes Here
  //-------------------------------------------------


  
  // Get Lepton Containers
  xAOD::ElectronContainer electrons = lepHandler()->getSelectedElectrons();
  xAOD::MuonContainer         muons = lepHandler()->getSelectedMuons();
  if ( electrons.size() == 0 && muons.size() == 0 ) return EL::StatusCode::SUCCESS;

  // Decide if electron or muon selection.... (t/f)
  bool useElectrons(true), useMuons(true);
  // xAOD::IParticleContainer leptons;
  // if (useElectrons) leptons = electrons;
  // else leptons = muons;

  // Some Histograms
  histoStore()->fillTH1F( "h_NPV", NPV(), weight() );
  histoStore()->fillTH1F( "h_averageMu", averageMu(), weight() );

  // Loops over particles and hits below copied directly from
  // "PIDTestingTool/Root/PIDAnalysis.cxx" without full understanding
  // -KSH
  // double w = (isData()) ? 1.0 : weight();

  if ( useElectrons ) {
      // Loop Over Electron
      for( auto electron: electrons ) {

          const xAOD::TrackParticle* track = lepHandler()->getTrack( electron );
          if ( not lepHandler()->passSelection( track ) ) continue;

          histoStore()->fillTH1F("h_trkPt", track->pt()*TRT::invGeV, weight());
          histoStore()->fillTH2F("h_pt_eta_el", track->eta(), track->pt()*TRT::invGeV, weight());

          // Info("execute()","pt: %f",track->pt()*TRT::invGeV);
          // Info("execute()","eta: %f",track->eta());
      }
  }

  if ( useMuons ) {
      // Loop Over Muons
      for( auto muon: muons ) {

          const xAOD::TrackParticle* track = lepHandler()->getTrack( muon );
          if ( not lepHandler()->passSelection( track ) ) continue;

          histoStore()->fillTH1F("h_trkPt", track->pt()*TRT::invGeV, weight());
          histoStore()->fillTH2F("h_pt_eta_mu", track->eta(), track->pt()*TRT::invGeV, weight());

          // Info("execute()","pt: %f",track->pt()*TRT::invGeV);
          // Info("execute()","eta: %f",track->eta());
      }
  }

  return EL::StatusCode::SUCCESS;
}
