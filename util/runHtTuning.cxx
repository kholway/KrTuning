#include "KrTuning/KrPhtHists.h"
#include "TRTFramework/RunUtils.h"

// A duplicate of run KrPhtHists for now
int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  KrPhtHists *alg = new KrPhtHists();

  // Use helper to start the job
  TRT::runJob(alg, argc, argv);

  return 0;
}
